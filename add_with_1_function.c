//Write a program to add two user input numbers using one function.
#include <stdio.h>


int main()

{
    int num1, num2, sum;

    printf("enter two numbers:\n");
    scanf("%d %d" , &num1, &num2);
    printf("Entered numbers are:%d and %d\n", num1, num2);

    sum = num1+num2;
    printf("sum of %d and %d is: %d\n", num1, num2, sum);

    return 0;
}

